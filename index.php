<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");

echo "<h3 style='font-style:italic;border-bottom:solid 1px black;width:80px;'>Release 0</h3>";

echo "Nama Hewan: ";
echo $sheep->name."<br>"; // "shaun"
echo "Jumlah Kaki: ";
echo $sheep->get_legs()."<br>"; // 2
echo "Berdarah Dingin: ";
echo $sheep->get_cold_blooded()."<br>"; // false

echo "<h3 style='font-style:italic;border-bottom:solid 1px black;width:80px;'>Release 1</h3>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan: ";
echo $sungokong->name;
echo "<br>Jumlah Kaki: ";
echo $sungokong->get_legs();
echo "<br>Action: ";
$sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
echo "<br>Nama Hewan: ";
echo $kodok->name;
echo "<br>Jumlah Kaki: ";
echo $kodok->get_legs();
echo "<br>Action: ";
$kodok->jump() ; // "hop hop"

?>
