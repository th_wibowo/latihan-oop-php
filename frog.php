<?php

require_once('animal.php');

Class Frog extends Animal{
  protected $legs = 4;
  public function get_legs(){
     return $this->legs;
  }
  public function jump(){
    echo "\"hop hop\"";
  }
}

?>
